package com.amazon.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class BaseTest {

    public WebDriver driver;

    @BeforeMethod
    public void setUp(){

        //Check the opereting system and according to result (windows or mac machine) run the chromedriver
        String operSys = System.getProperty("os.name").toLowerCase();
        if (operSys.contains("win")) {
            System.setProperty("webdriver.chrome.driver", new File("src/driver/chromedriverWIN.exe").getAbsolutePath());
            driver = new ChromeDriver();
        } else {
            System.setProperty("webdriver.chrome.driver", new File("src/driver/chromedriverMAC").getAbsolutePath());
            driver = new ChromeDriver();
        }
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com");

    }

    @AfterTest
    public void close(){

        driver.close();
    }

}
