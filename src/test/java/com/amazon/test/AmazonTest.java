package com.amazon.test;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *     Test flow:
 *     1. Go to https://www.amazon.com
 *     2. Search for "hats for men"
 *     3. Add first hat to Cart with quantity 2
 *     4. Open cart and assert total price and quantity are correct
 *     5. Search for "hats for women"
 *     6. Add first hat to Cart with quantity 1
 *     7. Open cart and assert total price and quantity are correct
 *     8. Change the quantity for item selected at step 3 from 2 to 1 item in Cart
 *     9. Assert total price and quantity are changed correctly
 *
 */

public class AmazonTest extends BaseTest {

    @Test
    public void amazonTest() {

        int numberOfFirstAddedProduct = 2;
        int numberOfSecondAddedProduct = 1;
        int changedQuantityNumber = 1;
        
        AmazonPage amazonPage = new AmazonPage(driver);
        SearchResultPage searchResultPage = new SearchResultPage(driver);
        ProductPage productPage = new ProductPage(driver);
        CartPage cartPage = new CartPage(driver);

        //When amazon.com page is opened, put search criteria and click on Search button
        amazonPage.searchField("hats for men");
        amazonPage.submitButton();

        //Select first product from the list
        searchResultPage.selectFirstListedElement();

        //The page with that product is opened. Select quantity, save the first product price per unit and click on Cart button
        double firstProductPrice = productPage.getTheProductPrice();

        productPage.selectNumberOfProducts(Integer.toString(numberOfFirstAddedProduct));
        productPage.clickOnAddToCartButton();
        productPage.clickOnCartIcon();

        //Check that quantity is as expected. Check that quantity * price per unit is as expected
        Assert.assertEquals(Integer.toString(numberOfFirstAddedProduct), cartPage.getQuantityOfFirstProduct(), "Number of selected products is not the same");
        Assert.assertEquals((firstProductPrice * numberOfFirstAddedProduct), cartPage.getSubtotalPriceFromCart(), "The price is not correct");

        //Enter a new criteria in the search bar and click on search button
        amazonPage.searchField("hats for women");
        amazonPage.submitButton();//click on Submit button

        //The page with that product is opened. Select quantity, save the second product price per unit and click on Cart button
        searchResultPage.selectFirstListedElement();
        double secondProductPrice = productPage.getTheProductPrice();

        //Select the quantity, add to the cart and click on cart icon
        productPage.selectNumberOfProducts(Integer.toString(numberOfSecondAddedProduct));
        productPage.clickOnAddToCartButton();
        productPage.clickOnCartIcon();

        //Verify that selected quantity is as expected. Check that total price is as expected (price for the first product * quantity + price for second product * quantity)
        Assert.assertEquals(Integer.toString(numberOfSecondAddedProduct), cartPage.getQuantityOfFirstProduct(), "Number Of selected products is not the same");
        Assert.assertEquals((secondProductPrice * numberOfSecondAddedProduct + firstProductPrice * numberOfFirstAddedProduct), cartPage.getSubtotalPriceFromCart(), "Total price for both products is not correct");

        //Change the quantity for the first selected product.
        cartPage.changedQuantity(changedQuantityNumber);

        //Check that the total price is expected after the quantity of product has been changed
        Assert.assertEquals((firstProductPrice * changedQuantityNumber + secondProductPrice * numberOfSecondAddedProduct), cartPage.getSubtotalPriceFromCart(), "Total price for both products is not correct");

        //Check that number of products (total quantity is as expected
        Assert.assertEquals(cartPage.totalAmountOfSelectedItems(), numberOfSecondAddedProduct + changedQuantityNumber, "Total number of products in the cart is not correct.");
    }
}
