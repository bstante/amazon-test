package com.amazon.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ProductPage extends BasePage {

    double productPriceDouble = 0.0;

    By productPriceID = By.id("priceblock_ourprice");
    By quantinyOfProducts = By.id("quantity");
    By addToCardButton = By.id("add-to-cart-button");
    By cartIcon = By.id("nav-cart");

    public ProductPage(WebDriver driver) {

        this.driver = driver;
    }
    //Get the product price and remove currency sign
    public double getTheProductPrice() {
        return productPriceDouble = Double.parseDouble((driver.findElement(productPriceID).getText().substring(1)));
    }

    //Select quantity from the dropdown menu
    public void selectNumberOfProducts(String value) {
        WebElement numberOfProducts = driver.findElement(quantinyOfProducts);
        Select selectNumber = new Select(numberOfProducts);
        selectNumber.selectByValue(value);
    }

    public void clickOnAddToCartButton() {

        driver.findElement(addToCardButton).click();
    }

    public void clickOnCartIcon() {

        driver.findElement(cartIcon).click();
    }

}
