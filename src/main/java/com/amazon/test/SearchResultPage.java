package com.amazon.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchResultPage extends BasePage {

    By selectFirstElement = By.xpath("//div[@data-index='0']//div");

    public SearchResultPage(WebDriver driver) {

        this.driver = driver;
    }

    public void selectFirstListedElement() {
        driver.findElement(selectFirstElement).click();
    }
}
