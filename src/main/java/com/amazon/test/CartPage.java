package com.amazon.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class CartPage extends BasePage{

    double subtotalPrice = 0.0;
    int totalItems = 0;

    By subtotalCartPrice = By.id("sc-subtotal-amount-activecart");
    By quantityOfSecondProduct = By.id("a-autoid-2");
    By totalQuantityOfItems = By.id("sc-subtotal-label-activecart");

    public CartPage(WebDriver driver) {

        this.driver = driver;
    }

    public String getQuantityOfFirstProduct() {

        By quantityOfFirstProduct = By.id("a-autoid-0-announce");

        return (driver.findElement(quantityOfFirstProduct).getText());
    }

    //Get the total price from the cart. Remove currency letters
    public double getSubtotalPriceFromCart() {
        return subtotalPrice = Double.parseDouble(driver.findElement(subtotalCartPrice).getText().substring(4));
    }

    //This method will change the quantity for the second product in the list.
    public void changedQuantity(int changedQuantity) {

        WebElement dropdownQuantityMenu = driver.findElement(quantityOfSecondProduct);
        dropdownQuantityMenu.click();
        List<WebElement> allPossibleQuantities;
        allPossibleQuantities = driver.findElements(By.xpath("//div[@id='a-popover-4']//li//a"));

        for (WebElement quantnty : allPossibleQuantities) {
            if (String.valueOf(changedQuantity).equals(quantnty.getText()))
                quantnty.click();
        }

        driver.navigate().refresh();
    }

    public int totalAmountOfSelectedItems() {
        return totalItems = Integer.parseInt(driver.findElement(totalQuantityOfItems).getText().substring(10, 11));
    }
}
