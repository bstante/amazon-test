package com.amazon.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AmazonPage extends BasePage {

    By searchFieldLocator = By.id("twotabsearchtextbox");
    By searchButton = By.xpath("//input[@type='submit']");


    //constructor will initialize driver for this page
    public AmazonPage(WebDriver driver) {

        this.driver = driver;
    }

    public void searchField(String searchCriteria) {

        driver.findElement(searchFieldLocator).sendKeys(searchCriteria);
    }

    public void submitButton() {
        driver.findElement(searchButton).submit();
    }
}
